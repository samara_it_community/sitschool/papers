# 📋 Whitepaper: SITsChool

| **Description** | Develop and Deploy knowledge sharing platform for local IT communities.<br>Key Priorities:<br><br>- Inclusive - any participant from young student to senior specialist should be comfortable here<br>- Community oriented - main goal is to establish strong and positive connection between members<br>- Fast and lightweight - usage of modern technologies with PoC for speed and low resources utilization |
| --------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Status**      | Early 🌱                                                                                                                                                                                                                                                                                                                                                                                                        |
| **Team**        | Product Owner: @Никита П <br>Master of Code: @Артём Д                                                                                                                                                                                                                                                                                                                                                           |
| **Related**     | - [+[SITSchool] Requirements](https://paper.dropbox.com/doc/SITSchool-Requirements-ruBTTjEbHAh2yLZTs21tF)                                                                                                                                                                                                                                                                                                       |

# Details
## Problem Statement

Samara IT Community has a number of local experts willing to share their knowledge and “newbies” who try to get into industry. Chats and social networks has no convenient tools to help them establish connections and that’s where SITsChool can took it’s place.

----------
## Background

Most user-stories that I had in my experience with people willing to become Developer/QA Engineer/DevOps can be defined as a set of following steps:

1. What? They are trying to understand what is IT industry is about looking for people they know who already inside community. If they don’t have such a friend they will listen to some webinars or read another low-trust resource wanting to find the answers. So first issue is getting **first contact** to occur.
2. Where? And when they understand basic ideas, situation on the market and common skill-sets it’s time to move onto the next step - training. Unfortunately despite numerous universities, online schools and YouTube tutorials the hardest thing to achieve here is finding a good mentor. In our industry we usually put a lot of effort in self-education, yet we still share a lot of information inside community to make it more efficient, look at StackOveflow and it’s problem-solving oriented approach! This issue can be described as **establishing trustworthy relationships**.
3. How? Having community of people ready to help, no matter in exchange for money as a professional service or in a altruistic manner, trainee starts to use or develop their own methodology to master a profession. And here we get our final issue - **path’s discovery**.

To sum up we have three steps where we can improve user experience by providing right tools to do the right job.

----------


## Proposed Solution

Let’s try to define what we are going to build here:

> SITsChool is a set of tools integrated as one user-facing system. It’s goal to establish social network between local community members and newcomers. Open Source nature of our project means that people can contribute new functionality and content as well.


![Our core domain entities - knowledge, members and skills. Members aggregates skills, while knowledge aggregates both, skills and members.](https://paper-attachments.dropbox.com/s_A3AAEDC5F3F03CAB16B57BC87053A5EAC12C35D05FACA823173D1F4363151351_1636237601324_base_entities.svg)


**Knowledge**
Is an artifact which can be shared between community members. Course or article for example:

![Knowledge may have different representations like Article or a Course. Course at the same time may compose another knowledge.](https://paper-attachments.dropbox.com/s_A3AAEDC5F3F03CAB16B57BC87053A5EAC12C35D05FACA823173D1F4363151351_1636238111648_knowledge.svg)


**Member**
Represents Identity and it’s mostly a subject of interaction - Author of a Course, Reader of an Article, etc.

![Author, Reader, Student all members. They also can have complex relations with each other.](https://paper-attachments.dropbox.com/s_A3AAEDC5F3F03CAB16B57BC87053A5EAC12C35D05FACA823173D1F4363151351_1636238521476_member.svg)


**Skill**
Could be not presented directly, but connected as meta-information to Knowledge and Members.

![Skill represents different categories like programming languages, business related or “soft” skills.](https://paper-attachments.dropbox.com/s_A3AAEDC5F3F03CAB16B57BC87053A5EAC12C35D05FACA823173D1F4363151351_1636239547417_skill.svg)

----------

**System**
To promote interaction between members and reach our goals we should build a complex system.
Let’s decompose it to three categories of components (services):

- Core - key part of our system, like Courses Editor (as a part of Courses Management Subsystem), Writer (Application to easily write and share articles), Skills Browser (Ontology explorer for Data Engineers);
- Supporting - things that are important, yet not a part of our main domain, like Sharing Service (which will post Articles to Medium), EdI (integration with different Educational platforms, for example with EdX and Exercism.io);
- Generic - more infrastructure related things, like identity management, services discovery, DevSecTestOps, etc.
## Conclusion

This document provides initial information about a project and should be used as a starting point to become familiar with it. Absence of formal requirements was done on purpose to do not restrict any ideas which are very welcome! Please submit them directly to @Никита П 


## To-dos
[x] Add PoC’s Architecture Diagram into proposal @Никита П 
[ ] Publish to Git and ask for several Peer reviews @Никита П

