# Papers

Main concept and domain description + additional documents.

## Whitepaper

Main concept of SITsChool with core domain description.

## Roadmap

It will be presented when more requirements will come in and active development phase will start.

## Requirements

Functional and non-functional requirements are analyzed stakeholders needs. Should be mapped with issues in our projects.

## Authors and acknowledgment

Maintainer: Nikita Puzankov @humb1t

## Project status

Early planning.
